using System;
using System.IO;
using System.Net;
using System.Linq;
using System.Threading;
using System.Collections.Generic;
using Newtonsoft.Json.Linq;
using Core;
using MihaZupan;
using Telegram.Bot;
using Telegram.Bot.Args;
using Telegram.Bot.Types;
using Telegram.Bot.Types.ReplyMarkups;
using Telegram.Bot.Types.Enums;
using MessageType = Core.MessageType;

namespace TelegramClient
{
    public class Client
    {
        private ITelegramBotClient Bot { get; }
        private string ServerAddress { get; }

        public Client()
        {
            var jsonConfig = FileReader.SharedInstance.Read("../TelegramClient/config.json");
            var config = JObject.Parse(jsonConfig);

            ServerAddress = config["server_address"].ToString();
            
            var proxy = new HttpToSocks5Proxy(
                config["proxy"]["address"].ToString(), 
                Convert.ToInt32(config["proxy"]["port"]),
                config["proxy"]["login"].ToString(),
                config["proxy"]["password"].ToString()
            );
        
            Bot = new TelegramBotClient(config["token"].ToString(), proxy);
            Bot.OnMessage += ActOnMessage;
            Bot.OnCallbackQuery += ActOnCallbackQueryReceived;
        }

        public void Run()
        {
            Console.WriteLine("Starting...");
            Bot.StartReceiving();
            Thread.Sleep(int.MaxValue);
        }

        private async void ActOnCallbackQueryReceived(object sender, CallbackQueryEventArgs e)
            => MakeRequest(
                e.CallbackQuery.Message.Chat.Id,
                e.CallbackQuery.From.Id,
                e.CallbackQuery.Data,
                MessageType.InlineKeyboard
            );

        private async void ActOnMessage(object sender, MessageEventArgs e)
        {
            var message = e.Message;
            string messageText;
            if (message.Contact != null)
                messageText = message.Contact.PhoneNumber;
            else if (message.Location != null)
                messageText = $"{message.Location.Latitude}:{message.Location.Longitude}";
            else 
                messageText = message.Text;

            MakeRequest(e.Message.Chat.Id, e.Message.From.Id, messageText, MessageType.Text);
        }

        private async void MakeRequest(long chatId, long userId, string messageText, MessageType messageType)
        {
            if (string.IsNullOrEmpty(messageText)) return;
            var requestString = ServerAddress + $"?userId={userId}&message={messageText}&messageType={(int)messageType}";

            try
            {
                var request = (HttpWebRequest)WebRequest.Create(requestString);
                var response = (HttpWebResponse) await request.GetResponseAsync();
            
                using (var stream = response.GetResponseStream())
                using (var reader = new StreamReader(stream))
                {
                    var responseString = reader.ReadToEnd();
                    FetchResponse(chatId, responseString);
                }
                response.Close();
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
                await Bot.SendTextMessageAsync(chatId: chatId, text: Title.ServerError);
            }
            
        }

        private async void FetchResponse(long chatId, string responseString)
        {
            try
            {
                var jsonResponse = JObject.Parse(responseString);
                var inlineKeyboard = GetInlineKeyboard(jsonResponse["options"]);
                var text = jsonResponse["text"].ToString();
                if (inlineKeyboard == null) 
                    await Bot.SendTextMessageAsync(
                        chatId: chatId,
                        text: text,
                        parseMode: ParseMode.Markdown);
                else 
                    await Bot.SendTextMessageAsync(
                        chatId: chatId, 
                        text: text,
                        replyMarkup: inlineKeyboard,
                        parseMode: ParseMode.Markdown);
            }
            catch
            {
                await Bot.SendTextMessageAsync(chatId: chatId, text: Title.ErrorMessage);
            } 
        }

        private static InlineKeyboardMarkup GetInlineKeyboard(JToken options)
        {
            if (options == null) return null;
            
            var buttons = new List<List<InlineKeyboardButton>>();
            foreach (var option in options.Children())
            {
                if (option.ToString().Equals("[]"))
                    return null;
                buttons.Add( 
                    new List<InlineKeyboardButton>{ InlineKeyboardButton.WithCallbackData(option.ToString()) }
                );
            }

            return new InlineKeyboardMarkup(buttons);
        }
    }
}