﻿using System;

namespace TelegramClient
{
    class Program
    {
        static void Main(string[] args)
        {
            new Client().Run();
        }
    }
}
