using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using Core;
using Core.Models;
using Newtonsoft.Json.Linq;

namespace Domain
{
    
    public static class ResponseCreator
    {
        private static readonly Constants Constants = Constants.SharedInstance;
        
        public static string GetResponseString(string text, IEnumerable<string> options)
        {
            dynamic jsonResponse = new JObject();
            jsonResponse.text = text;
            jsonResponse.options = new JArray(options);
            return jsonResponse.ToString();
        }
        
        public static IEnumerable<DateTime> GetNextFiveDays()
        {
            var days = new List<DateTime>();
            var currentDay = DateTime.Now;
            for (var i = 1; i <= 7; i++)
                days.Add(currentDay.AddDays(i));
            return days.Where(day => !day.DayOfWeek.Equals(DayOfWeek.Saturday) && !day.DayOfWeek.Equals(DayOfWeek.Sunday));
        }
        
        public static string DetailedInfo(Order order)
        {
            var result = new StringBuilder();
            var detailedHeaders = Constants["DetailedOrder"];

            result.Append($"{detailedHeaders["BottleQuantity"]}{order.BottleQuantity}");
            result.Append($"{detailedHeaders["Date"]}{new DateTime(order.DeliveryDate).ToString("D", CultureInfo.CreateSpecificCulture("ru-RU"))}");
            result.Append($"{detailedHeaders["Time"]}{Constants["Time"][((Time)order.DeliveryTimePeriod).ToString()]}");
            result.Append($"{detailedHeaders["Address"]}https://maps.google.com?daddr={order.Address.Latitude},{order.Address.Longitude}");
            result.Append($"{detailedHeaders["Phone"]}{order.PhoneNumber}");
            
            return result.ToString();
        }

        public static string ActiveOrdersList(IEnumerable<Order> orders)
        {
            try
            {
                var result = new StringBuilder();
                var detailedHeaders = Constants["DetailedOrder"];
                result.Append($"{Title.ActiveOrders}");

                using (var context = new ApplicationContext())
                {
                    foreach (var order in orders)
                    {
                        var currentAddress = context.Addresses.First(address => address.Id == order.Id);
                        result.Append("\n");
                        result.Append($"{detailedHeaders["OrderId"]}{order.Id}");
                        result.Append($"{detailedHeaders["BottleQuantity"]}{order.BottleQuantity}");
                        result.Append($"{detailedHeaders["Date"]}{new DateTime(order.DeliveryDate).ToString("D", CultureInfo.CreateSpecificCulture("ru-RU"))}");
                        result.Append($"{detailedHeaders["Time"]}{Constants["Time"][((Time)order.DeliveryTimePeriod).ToString()]}");
                        result.Append($"{detailedHeaders["Address"]}https://maps.google.com?daddr={currentAddress.Latitude},{currentAddress.Longitude}");
                        result.Append($"{detailedHeaders["Phone"]}{order.PhoneNumber}");
                    }
                }

                return result.ToString();
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                return Title.WrongState;
            }
        }
    }
}