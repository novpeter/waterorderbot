using System.IO;
using System.Text;
using Core;
using Core.Models;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json.Linq;

namespace Domain
{
    public class ApplicationContext : DbContext
    {
        public DbSet<Order> Orders { get; set; }
        public DbSet<Address> Addresses { get; set; }

        public ApplicationContext(){ }
        
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            var config = JObject.Parse(FileReader.SharedInstance.Read("../Domain/config.json"));

            var configString = new StringBuilder();
            configString.Append($"Host={config["Host"]};");
            configString.Append($"Port={config["Port"]};");
            configString.Append($"Database={config["Database"]};");
            configString.Append($"Username={config["Username"]};");
            configString.Append($"Password={config["Password"]};");
            configString.Append($"SslMode={config["SslMode"]};");
            configString.Append($"Trust Server Certificate={config["TrustServerCertificate"]}");
                
            optionsBuilder.UseNpgsql(configString.ToString()); 
        }
    }
}