using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Core;
using Core.Models;
using Microsoft.AspNetCore.Server.Kestrel.Core;
using Newtonsoft.Json.Linq;

namespace Domain
{
    public static class RequestHandler
    {        
        private static readonly Dictionary<long, State> BotStates = new Dictionary<long, State>();
        
        public static string HandleRequest(HttpListenerRequest request)
        {
            var queries = request.QueryString;
            var queryKeys = queries.AllKeys;

            if (!queryKeys.Contains("userId") || !queryKeys.Contains("message") || !queryKeys.Contains("messageType"))
                return ResponseCreator.GetResponseString(Title.ErrorMessage,  new string[]{});
            
            try
            {
                var userId = (long) Convert.ToDouble(queries["userId"]);
                var message = queries["message"];
                var messageType = queries["messageType"];
                return HandleMessage(userId, message, messageType);
            }
            catch
            {
                return ResponseCreator.GetResponseString(Title.WrongState, new string[]{});
            }
        }

        private static string HandleMessage(long userId, string message, string messageType)
        {
            if (!BotStates.ContainsKey(userId)) BotStates[userId] = State.Start;
            
            switch (message)
            {
                case "/start":
                    BotStates[userId] = State.Start;
                    return ResponseCreator.GetResponseString(Title.Start, Utils.GetStateOptions(OrderState.Start));
                case "/info":
                    BotStates[userId] = State.Info;
                    return ResponseCreator.GetResponseString(Title.Info, new string[] { });
                default: break;
            }

            switch (BotStates[userId])
            {
                case State.Start:
                    if (Convert.ToInt32(messageType) == (int) MessageType.InlineKeyboard)
                    {
                        var startScreenOptions = Utils
                            .GetStateOptions(OrderState.Start)
                            .Select(option => option.ToString())
                            .ToList();

                        if (startScreenOptions[0].Equals(message))
                        {
                            BotStates[userId] = State.CreateOrder;
                            DataManager.CreateOrder(userId);
                            return ResponseCreator.GetResponseString(
                                Title.SetBottleCount,
                                Utils.GetStateOptions(OrderState.SetBottleCount));
                        }

                        if (startScreenOptions[1].Equals(message))
                        {
                            BotStates[userId] = State.DisplayActiveOrders;
                            return HandleGetActiveOrders(userId);
                        }
                    }
                    else
                    {
                        return ResponseCreator.GetResponseString(Title.Start, Utils.GetStateOptions(OrderState.Start));
                    }
                    break;
                case State.Info:
                    return ResponseCreator.GetResponseString(Title.Info, new string[] { });
                case State.DisplayActiveOrders:
                    if (Convert.ToInt32(messageType) == (int) MessageType.InlineKeyboard)
                    {
                        var activeOrdersOptions = Utils
                            .GetStateOptions(OrderState.ActiveOrders)
                            .Select(option => option.ToString())
                            .ToList();

                        if (activeOrdersOptions[0].Equals(message))
                        {
                            BotStates[userId] = State.CreateOrder;
                            DataManager.CreateOrder(userId);
                            return ResponseCreator.GetResponseString(
                                Title.SetBottleCount,
                                Utils.GetStateOptions(OrderState.SetBottleCount));
                        }

                        if (activeOrdersOptions[1].Equals(message))
                        {
                            BotStates[userId] = State.DeleteOrder;
                            return PrepareOrderDeleting(userId);
                        }
                    }
                    break;
                case State.CreateOrder:
                    if (DataManager.HasNotCompletedOrder(userId))
                    {
                        var order = DataManager.GetNotCompletedOrder(userId);
                        switch (order.State)
                        {
                            case (int)OrderState.SetBottleCount:
                                return HandleSetBottleCount(userId, order, message);
                            case (int)OrderState.SetDate:
                                return HandleSetDate(order, message, messageType);
                            case (int)OrderState.SetTime:
                                return HandleSetTime(order, message, messageType);
                            case (int)OrderState.SetAddress:
                                return HandleSetAddress(order, message);
                            case (int)OrderState.SetPhone:
                                return HandleSetPhone(order, message);
                            case (int)OrderState.Confirm:
                                return HandleConfirm(userId, order, message, messageType);
                            default: break;
                        }
                    }
                    break;
                case State.DeleteOrder:
                    if (Convert.ToInt32(messageType) == (int) MessageType.InlineKeyboard)
                    {
                        return HandleDeleteOrder(userId, message);
                    }
                    break;
            }
            return ResponseCreator.GetResponseString(Title.WrongState, new string[]{});
            
        }

        private static string HandleGetActiveOrders(long userId)
        {
            var orders = DataManager.GetActiveOrders(userId);
            return orders.Count == 0 
                ? ResponseCreator.GetResponseString(Title.NoActiveOrders, Utils.GetStateOptions(OrderState.Start)) 
                : ResponseCreator.GetResponseString(
                    ResponseCreator.ActiveOrdersList(orders), 
                    Utils.GetStateOptions(OrderState.ActiveOrders)) ;
        }

        private static string PrepareOrderDeleting(long userId)
        {
            var activeOrders = DataManager.GetActiveOrders(userId);
            return activeOrders.Count == 0 
                ? ResponseCreator.GetResponseString(Title.NoActiveOrders, Utils.GetStateOptions(OrderState.Start)) 
                : ResponseCreator.GetResponseString(
                    Title.DeleteOrder, 
                    activeOrders.Select(order => order.Id.ToString()));
        }

        private static string HandleDeleteOrder(long userId, string orderId)
        {            
            BotStates[userId] = State.Start;
            return DataManager.DeleteOrder(userId, orderId) 
                ? ResponseCreator.GetResponseString(Title.OrderDeleted, Utils.GetStateOptions(OrderState.Start))
                : ResponseCreator.GetResponseString(Title.WrongState, Utils.GetStateOptions(OrderState.Start));
        }

        #region handle order creation

        private static string HandleSetBottleCount(long userId, Order order, string message)
        {
            var options = ResponseCreator
                .GetNextFiveDays()
                .Select(date => date.ToString("D", CultureInfo.CreateSpecificCulture("ru-RU")));

            return DataManager.SetBottleCount(userId, order, message) 
                ? ResponseCreator.GetResponseString(Title.SetDate, options)
                : ResponseCreator.GetResponseString(
                    Title.WrongInput + Title.SetBottleCount,
                    Utils.GetStateOptions(OrderState.SetBottleCount));
        }
        
        private static string HandleSetDate(Order order, string message, string messageType)
        {
            var options = ResponseCreator
                .GetNextFiveDays()
                .Select(date => date.ToString("D", CultureInfo.CreateSpecificCulture("ru-RU")));
            
            if (Convert.ToInt32(messageType) == (int) MessageType.Text)
                return ResponseCreator.GetResponseString(Title.WrongInput + Title.SetDate, options);
            
            return DataManager.SetDate(order, message)
                ? ResponseCreator.GetResponseString(Title.SetTime, Utils.GetStateOptions(OrderState.SetTime))
                : ResponseCreator.GetResponseString(Title.WrongInput + Title.SetDate, options);
        }
        
        private static string HandleSetTime(Order order, string message, string messageType)
        {
            if (Convert.ToInt32(messageType) == (int) MessageType.Text)
                return ResponseCreator.GetResponseString(
                    Title.WrongInput + Title.SetTime,
                    Utils.GetStateOptions(OrderState.SetTime));

            return DataManager.SetTime(order, message)
                ? ResponseCreator.GetResponseString(Title.SetAddress, Utils.GetStateOptions(OrderState.SetAddress))
                : ResponseCreator.GetResponseString(
                    Title.WrongInput + Title.SetTime,
                    Utils.GetStateOptions(OrderState.SetTime));
        }
        
        private static string HandleSetAddress(Order order, string message)
        {
            return DataManager.SetAddress(order, message)
                ? ResponseCreator.GetResponseString(Title.SetPhone, Utils.GetStateOptions(OrderState.SetPhone))
                : ResponseCreator.GetResponseString(
                    Title.WrongInput + Title.SetAddress,
                    Utils.GetStateOptions(OrderState.SetAddress));
        }

        private static string HandleSetPhone(Order order, string message)
        {
            return DataManager.SetPhoneNumber(order, message)
                ? ResponseCreator.GetResponseString(Title.Confirm + ResponseCreator.DetailedInfo(order), Utils.GetStateOptions(OrderState.Confirm))
                : ResponseCreator.GetResponseString(
                    Title.WrongInput + Title.SetPhone,
                    Utils.GetStateOptions(OrderState.SetPhone));
        }
        
        private static string HandleConfirm(long userId, Order order, string message, string messageType)
        { 
            if (Convert.ToInt32(messageType) == (int) MessageType.Text)
                return ResponseCreator.GetResponseString(
                    Title.WrongInput + Title.Confirm + ResponseCreator.DetailedInfo(order),
                    Utils.GetStateOptions(OrderState.Confirm));
            
            BotStates[userId] = State.Start;
            switch (DataManager.ConfirmOrder(userId, order, message))
            {
                case ConfirmOrderResult.Saved:
                    return ResponseCreator.GetResponseString(Title.Confirmed, Utils.GetStateOptions(OrderState.Start));
                case ConfirmOrderResult.Reset:
                    return ResponseCreator.GetResponseString(Title.Reset, new string[]{});
                default:
                    return ResponseCreator.GetResponseString(Title.WrongState, new string[]{});;
            }
        }

        #endregion
        
    }
}