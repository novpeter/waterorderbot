﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text.RegularExpressions;
using Core;
using Core.Models;
using Microsoft.EntityFrameworkCore.Internal;

namespace Domain
{    
   
    public static class DataManager
    {
        private static readonly Dictionary<long, Order> NotCompletedOrders = new Dictionary<long, Order>();

        public static List<Order> GetActiveOrders(long userId)
        {
            try
            {
                using (var context = new ApplicationContext())
                {
                    var currentDate = DateTime.Now.Subtract(new TimeSpan(1, 0, 0, 0)).Ticks;
                    var activeOrders = context
                        .Orders
                        .Where(order => order.UserId == userId && order.DeliveryDate >= currentDate)
                        .Take(5)
                        .ToList();
                    return activeOrders;
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                return new List<Order>();
            }
        }

        public static bool DeleteOrder(long userId, string orderId)
        {
            try
            {
                var currentOrderId = (long) Convert.ToDouble(orderId);
                using (var context = new ApplicationContext())
                {
                    var activeOrder = context
                        .Orders
                        .First(order => order.UserId == userId && order.Id == currentOrderId);

                    if (activeOrder == null) return true;
                    context.Orders.Remove(activeOrder);
                    context.SaveChanges();
                    return true;
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                return false;
            }
        }
        
        public static void CreateOrder(long userId)
        {
            var order = new Order {UserId = userId, State = (int) OrderState.SetBottleCount};
            NotCompletedOrders[userId] = order;
        }

        public static bool HasNotCompletedOrder(long userId) => NotCompletedOrders.ContainsKey(userId);

        public static Order GetNotCompletedOrder(long userId) => NotCompletedOrders[userId];

        public static bool SetBottleCount(long userId, Order order, string bottleCount)
        {
            try
            {
                var quantity = Convert.ToInt32(bottleCount);
                if (quantity > 20) return false;
                order.BottleQuantity = quantity;
                order.State = (int) OrderState.SetDate;
                NotCompletedOrders[userId] = order;
                return true;
            }
            catch (FormatException e)
            {
                Console.WriteLine(e.ToString());
                return false;
            }
        }

        public static bool SetDate(Order order, string date)
        {
            try
            {
                var selectedDate = DateTime.ParseExact(date, "D", CultureInfo.CreateSpecificCulture("ru-RU"));
                order.DeliveryDate = selectedDate.Ticks;
                order.State = (int) OrderState.SetTime;
                return true;
            }
            catch (FormatException e)
            {
                Console.WriteLine(e.ToString());
                return false;
            }
        }

        public static bool SetTime(Order order, string time)
        {
            var timeOptions = Utils.GetStateOptions(OrderState.SetTime);
            var optionIndex = timeOptions.IndexOf(time);
            if (optionIndex == -1) return false;
            order.DeliveryTimePeriod = optionIndex;
            order.State = (int) OrderState.SetAddress;
            return true;
        }

        public static bool SetAddress(Order order, string address)
        {
            if (string.IsNullOrEmpty(address)) return false;
            try
            {
                var coordinates = address
                    .Split(':')
                    .ToList();
                var deliveryAddress = new Address
                {
                    Latitude = Convert.ToDouble(coordinates[0]), 
                    Longitude = Convert.ToDouble(coordinates[1])
                };
                order.Address = deliveryAddress;
                order.State = (int) OrderState.SetPhone;
                return true;
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                return false;
            }
        }

        public static bool SetPhoneNumber(Order order, string phoneNumber)
        {
            if (string.IsNullOrEmpty(phoneNumber)) return false;
            try
            {
                var currentPhone = String.Concat(phoneNumber.Where(Char.IsDigit));
                if (currentPhone.Length < 10) return false;
                order.PhoneNumber = currentPhone;
                order.State = (int) OrderState.Confirm;
                return true;
            }
            catch (FormatException e)
            {
                Console.WriteLine(e);
                return false;
            }
        }

        public static ConfirmOrderResult ConfirmOrder(long userId, Order order, string message)
        {
            var options = Utils.GetStateOptions(OrderState.Confirm).ToList();
            if (options[0].Equals(message))
            {
                try
                {
                    using (var context = new ApplicationContext())
                    {
                        order.CreatedDateTime = DateTime.Now.Ticks;
                        order.State = (int) OrderState.Confirmed;
                        context.Addresses.Add(order.Address);
                        context.SaveChanges();
                        context.Orders.Add(order);
                        context.SaveChanges();
                        NotCompletedOrders.Remove(userId);
                        return ConfirmOrderResult.Saved;
                    }
                }
                catch (Exception e)
                {
                    Console.WriteLine(e);
                    return ConfirmOrderResult.Error;
                }
                
            }
            if (!options[1].Equals(message)) return ConfirmOrderResult.Error;
            
            NotCompletedOrders.Remove(userId);
            return ConfirmOrderResult.Reset;
        }
    }
}