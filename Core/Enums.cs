namespace Core
{
    public enum Time
    {
        Morning = 0,
        Afternoon = 1,
        Evening = 2
    }

    public enum OrderState
    {
        Start = 0,
        SetBottleCount = 1,
        SetDate = 2,
        SetTime = 3,
        SetAddress = 4,
        SetPhone = 5,
        Confirm = 6,
        Confirmed = 7,
        ActiveOrders = 8
    }

    public enum MessageType
    {
        Text = 0,
        InlineKeyboard = 1
    }
    
    public enum ConfirmOrderResult
    {
        Saved,
        Reset,
        Error,
    }

    public enum State
    {
        Start,
        Info,
        DisplayActiveOrders,
        DeleteOrder,
        CreateOrder
    }

}