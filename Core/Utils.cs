using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using Newtonsoft.Json.Linq;

namespace Core
{
    
    public static class Utils
    {
        private static Constants Constants => Constants.SharedInstance;

        public static IEnumerable<string> GetStateOptions(OrderState orderState)
        { 
            switch (orderState)
            {
                case OrderState.Start:
                    return Constants["OptionsToState"]["Start"]
                        .Children()
                        .Select(option => option.ToString());
                case OrderState.ActiveOrders:
                    return Constants["OptionsToState"]["ActiveOrders"]
                        .Children()
                        .Select(option => option.ToString());
                case OrderState.SetTime:
                    return Constants["OptionsToState"]["SetTime"]
                        .Children()
                        .Select(option => option.ToString());
                case OrderState.Confirm:
                    return Constants["OptionsToState"]["Confirm"]
                        .Children()
                        .Select(option => option.ToString());
                default: return new string[]{};
            }
        }
    }
}