namespace Core.Models
{
    public class Order
    {
        public int Id { get; set; }
        public long UserId { get; set; }
        public int BottleQuantity { get; set; }
        public long DeliveryDate { get; set; }
        public int DeliveryTimePeriod { get; set; }
        public int AddressId { get; set; }
        public Address Address { get; set; }
        public string PhoneNumber { get; set; }
        public int State { get; set; }
        public long CreatedDateTime { get; set; }
    }
}