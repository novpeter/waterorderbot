using System.IO;
using Newtonsoft.Json.Linq;

namespace Core
{
    public class Constants
    {
        public static readonly Constants SharedInstance = new Constants();
        private JObject Dictionary { get; }

        private Constants()
        {
            var jsonConstants = FileReader.SharedInstance.Read("../Core/Constants.json");
            Dictionary = JObject.Parse(jsonConstants);
        }
        
        public JToken this[string key] 
            => Dictionary.ContainsKey(key) ? Dictionary[key] : string.Empty;
    }

    public static class Title
    {
        private static Constants Constants => Constants.SharedInstance;
        private static JToken Titles => Constants["Titles"];

        public static string Start => Titles["Start"].ToString();
        public static string SetBottleCount => Titles["SetBottleCount"].ToString();
        public static string SetDate => Titles["SetDate"].ToString();
        public static string SetTime => Titles["SetTime"].ToString();
        public static string SetAddress => Titles["SetAddress"].ToString();
        public static string SetPhone => Titles["SetPhone"].ToString();
        public static string Confirm => Titles["Confirm"].ToString();
        public static string Confirmed => Titles["Confirmed"].ToString();
        public static string Reset => Titles["Reset"].ToString();
        public static string Info => Titles["Info"].ToString();
        public static string WrongInput => Titles["WrongInput"].ToString();
        public static string WrongState => Titles["WrongState"].ToString();
        public static string ServerError => Titles["ServerError"].ToString();
        public static string ActiveOrders => Titles["ActiveOrders"].ToString();
        public static string NoActiveOrders => Titles["NoActiveOrders"].ToString();
        public static string DeleteOrder => Titles["DeleteOrder"].ToString();
        public static string OrderDeleted => Titles["OrderDeleted"].ToString();
        
        public static string ErrorMessage => Constants["ErrorMessage"].ToString();
    }
}