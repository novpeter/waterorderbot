using System.IO;

namespace Core
{
    public class FileReader
    {
        public static readonly FileReader SharedInstance = new FileReader();
        
        private FileReader(){ }

        public string Read(string path)
        {
            try
            {
                using (var stream = File.OpenRead(path))
                {
                    var array = new byte[stream.Length];
                    stream.Read(array, 0, array.Length);
                    return System.Text.Encoding.Default.GetString(array);
                }
            }
            catch
            {
                return string.Empty;
            }
        }
    }
}