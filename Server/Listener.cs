using System;
using System.IO;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Domain;

namespace Server
{
    public class Listener
    {
        private string Url { get; }
        private string Port { get; }
        private string Prefix => $"{Url}:{Port}/";
        
        private readonly HttpListener _listener;

        public Listener(string url, string port)
        {
            Url = url;
            Port = port;
            
            _listener = new HttpListener();            
            _listener.Prefixes.Add(Prefix);
            _listener.Start();
        }

        public void Start()
        {
            Console.WriteLine("Listening...");
            Listen().Wait();
        }

        private async Task Listen()
        {
            _listener.Start();

            while (true)
            {
                var context = await _listener.GetContextAsync();
                var request = context.Request;
                var response = context.Response;

                Task.Run(() =>
                {
                    var responseString = RequestHandler.HandleRequest(request);

                    var buffer = System.Text.Encoding.UTF8.GetBytes(responseString);
                    response.ContentLength64 = buffer.Length;

                    using (var output = response.OutputStream)
                    {
                        output.Write(buffer, 0, buffer.Length);
                    }
                });
            }
        }
    }
}