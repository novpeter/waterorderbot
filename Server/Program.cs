﻿using System.IO;
using Core;
using Domain;
using Newtonsoft.Json.Linq;

namespace Server
{
    class Program
    {
        static void Main(string[] args)
        {      
            var jsonConfig = FileReader.SharedInstance.Read("../Server/config.json");
            var config = JObject.Parse(jsonConfig);

            var url = config["url"].ToString();
            var port = config["port"].ToString();
            
            new Listener(url, port).Start();
        }
    }
}
